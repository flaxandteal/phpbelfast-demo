## Laravel Example for Gitlab-CI and AWS ECR

Using the `.gitlab-ci.yml` file in this directory, the necessary steps are run to build, test and deploy to an
AWS Elastic Container Repository defined in your Gitlab Variables:

- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_ECR_URI
- AWS_SECRET_ACCESS_KEY

Within the `containers/k8s` directory are the YAML definitions required to set up your own cluster.
